<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="/tags/loushang-web" prefix="l"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>dag</title>
<link rel="stylesheet" type="text/css"
	href="<l:asset path='css/bootstrap.css'/>" />
<style type="text/css">
.container {
	width: 100%;
}
</style>
<script type="text/javascript" src="<l:asset path='jquery.js'/>"></script>
<script type="text/javascript" src="<l:asset path='bootstrap.js'/>"></script>
<script type="text/javascript" src="<l:asset path='echarts-all.js'/>"></script>
<script type="text/javascript">
//项目上下文
var context="<%=request.getContextPath()%>";
</script>
</head>
<body>
	<div class="topdist"></div>
	<div class="container">
		<div id="main" style="width: 600px; height: 400px;"></div>
	</div>
	<script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        option = {
        	    title : {
        	        text: '数据转换流程图',
        	        x:'right',
        	        y:'bottom'
        	    },
        	    tooltip : {
        	        trigger: 'item',
        	        formatter: '{a} : {b}'
        	    },
        	    toolbox: {
        	        show : true,
        	        feature : {
        	            restore : {show: true},
        	            magicType: {show: true, type: ['force', 'chord']},
        	            saveAsImage : {show: true}
        	        }
        	    },
        	    legend: {
        	        x: 'left',
        	        data:['结束节点','普通节点']
        	    },
        	    series : [
        	        {
        	            type:'force',
        	            name : "流转",
        	            ribbonType: false,
        	            categories : [
        	                {
        	                    name: '结束节点'
        	                },
        	                {
        	                    name: '普通节点'
        	                }
        	            ],
        	            itemStyle: {
        	                normal: {
        	                    label: {
        	                        show: true,
        	                        textStyle: {
        	                            color: '#333'
        	                        }
        	                    },
        	                    nodeStyle : {
        	                        brushType : 'both',
        	                        borderColor : 'rgba(255,215,0,0.4)',
        	                        borderWidth : 1
        	                    }
        	                },
        	                emphasis: {
        	                    label: {
        	                        show: false
        	                        // textStyle: null      // 默认使用全局文本样式，详见TEXTSTYLE
        	                    },
        	                    nodeStyle : {
        	                        //r: 30
        	                    },
        	                    linkStyle : {}
        	                }
        	            },
        	            minRadius : 15,
        	            maxRadius : 25,
        	            gravity: 1.1,
        	            scaling: 1.2,
        	            draggable: false,
        	            linkSymbol: 'arrow',
        	            steps: 10,
        	            coolDown: 0.9,
        	            //preventOverlap: true,
        	            nodes:[
           	                //{category:0, name: '乔布斯', value : 10},
        	            ],
        	            links : [
        	                //{source : '丽萨-乔布斯', target : '乔布斯'},
        	            ]
        	        }
        	    ]
        	};

        function focus(param) {
            var data = param.data;
            var links = option.series[0].links;
            var nodes = option.series[0].nodes;
            if (
                data.source != null
                && data.target != null
            ) { //点击的是边
                var sourceNode = nodes.filter(function (n) {return n.name == data.source})[0];
                var targetNode = nodes.filter(function (n) {return n.name == data.target})[0];
                console.log("选中了边 " + sourceNode.name + ' -> ' + targetNode.name + ' (' + data.weight + ')');
            } else { // 点击的是点
                console.log("选中了" + data.name + '(' + data.value + ')');
            }
        }
        myChart.on('series.force', focus)

        myChart.on('series.force', function () {
            console.log(myChart.chart.force.getPosition());
        });
        $(document).ready(function() {
        	myChart.showLoading();
        	var url = context + "/service/web/task/dag";
        	//var json = JSON.stringify("");
        	$.ajax({
        		url : url,
        		type : "GET",
        		async : false,
        		contentType: "application/json",
        		dataType: "json",
        		data: "",
        		success : function(data){
        			if(data.success == true){
        		        for(var i=0;i<data.data.length;i++){
        		        	var o = data.data[i];
            				option.series[0].nodes.push({category : o.isEnd=='1' ? 0 : 1, name: o.taskName, value : 30});
        		        	option.series[0].links.push({source : o.preTaskName, target : o.taskName});
        		        }
        				myChart.hideLoading();
        				myChart.setOption(option);
        			}
        		},
        		error : function(data, textstatus){
    				myChart.hideLoading();
    				myChart.setOption(option);
        			alert(textstatus);
        		}
        	}); 
        });
    </script>
</body>
</html>