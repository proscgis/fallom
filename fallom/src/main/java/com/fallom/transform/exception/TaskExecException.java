package com.fallom.transform.exception;

public class TaskExecException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public TaskExecException() {
		super();

	}

	public TaskExecException(String message, Throwable cause) {
		super(message, cause);

	}

	public TaskExecException(String message) {
		super(message);

	}

	public TaskExecException(Throwable cause) {
		super(cause);

	}

}
