package com.fallom.transform.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcHelper {

	public static Connection getConn() throws ClassNotFoundException, SQLException {
		return conn108();
	}

	private static Connection conn108() throws ClassNotFoundException, SQLException {
		String url = "jdbc:oracle:thin:@10.110.1.108:1521:orcl";
		String user = "gsyw_test";
		String passwd = "111111";
		Connection conn = null;
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection(url, user, passwd);
		return conn;
	}

	private static Connection connhngs() throws ClassNotFoundException, SQLException {
		String url = "jdbc:oracle:thin:@RJZF-SCJG-38:1521:orcl";
		String user = "hngs";
		String passwd = "hngs";
		Connection conn = null;
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection(url, user, passwd);
		return conn;
	}

	public static void closeConn(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
