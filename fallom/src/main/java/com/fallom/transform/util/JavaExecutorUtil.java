package com.fallom.transform.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import com.fallom.transform.engine.TaskExecutor;

public class JavaExecutorUtil {
	/**
	 * @param sourceStr
	 *            Java源代码
	 */
	public static TaskExecutor getActExecutor(String sourceStr) throws Exception {
		// 类名及文件名
		String clsName = "Hello";
		// 当前编译器
		JavaCompiler cmp = ToolProvider.getSystemJavaCompiler();
		// Java标准文件管理器
		StandardJavaFileManager fm = cmp.getStandardFileManager(null, null, null);
		// Java文件对象
		JavaFileObject jfo = new StringJavaObject(clsName, sourceStr);
		// 编译参数，类似于javac <options>中的options
		List<String> optionsList = new ArrayList<String>();
		// 编译文件的存放地方
		optionsList.addAll(Arrays.asList("-d", JavaExecutorUtil.class.getResource("/").getPath()));
		// 要编译的单元
		List<JavaFileObject> jfos = Arrays.asList(jfo);
		// 设置编译环境
		JavaCompiler.CompilationTask task = cmp.getTask(null, fm, null, optionsList, null, jfos);
		// 编译成功
		if (task.call()) {
			// 生成对象
			Object obj = Class.forName(clsName).newInstance();
			if (obj instanceof TaskExecutor) {
				return (TaskExecutor) obj;
			}
		}
		return null;
	}

	/**
	 * @param sourceStr
	 *            Java源代码
	 */
	public static void test(String sourceStr) throws Exception {
		// 类名及文件名
		String clsName = "Hello";
		// 方法名
		String methodName = "sayHello";
		// 当前编译器
		JavaCompiler cmp = ToolProvider.getSystemJavaCompiler();
		// Java标准文件管理器
		StandardJavaFileManager fm = cmp.getStandardFileManager(null, null, null);
		// Java文件对象
		JavaFileObject jfo = new StringJavaObject(clsName, sourceStr);
		// 编译参数，类似于javac <options>中的options
		List<String> optionsList = new ArrayList<String>();
		// 编译文件的存放地方
		optionsList.addAll(Arrays.asList("-d", JavaExecutorUtil.class.getResource("/").getPath()));
		// 要编译的单元
		List<JavaFileObject> jfos = Arrays.asList(jfo);
		// 设置编译环境
		JavaCompiler.CompilationTask task = cmp.getTask(null, fm, null, optionsList, null, jfos);
		// 编译成功
		if (task.call()) {
			// 生成对象
			Object obj = Class.forName(clsName).newInstance();
			Class<? extends Object> cls = obj.getClass();
			// 调用sayHello方法
			Method m = cls.getMethod(methodName, String.class);
			String str = (String) m.invoke(obj, "Dynamic Compilation");
			System.out.println(str);
		}

	}

}
