package com.fallom.transform.engine;

import java.util.HashMap;
import java.util.Map;

public class TaskResult {
	private String id;
	private String taskId;
	private String jobId;
	private boolean isSuccess;
	// 执行开始时间
	private long execStartTime;
	// 执行结束时间
	private long execEndTime;
	private Map<String, Object> result = new HashMap<String, Object>();
	private Task task;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	/**
	 * 获取所有执行结果
	 * 
	 * @return
	 */
	public Map<String, Object> getResult() {
		return result;
	}

	/**
	 * 保存执行结果
	 * 
	 * @param key
	 * @param result
	 */
	public void setResult(String key, Object result) {
		this.result.put(key, result);
	}

	/**
	 * 获取错误信息
	 * 
	 */
	public String getResultErrorMessage() {
		return (String) this.result.get(TaskResultConstant.RESULT_KEY_ERROR_MESSAGE);
	}

	/**
	 * 保存错误信息
	 * 
	 * @param errorMessage
	 */
	public void setResultErrorMessage(String errorMessage) {
		this.result.put(TaskResultConstant.RESULT_KEY_ERROR_MESSAGE, errorMessage);
	}

	public static final class TaskResultConstant {
		public static final String RESULT_KEY_ERROR_MESSAGE = "ERROR_MESSAGE";
		public static final String RESULT_KEY_RETURN_VALUE = "RETURN_VALUE";
	}

	public long getExecStartTime() {
		return execStartTime;
	}

	public void setExecStartTime(long execStartTime) {
		this.execStartTime = execStartTime;
	}

	public long getExecEndTime() {
		return execEndTime;
	}

	public void setExecEndTime(long execEndTime) {
		this.execEndTime = execEndTime;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.taskId = task.getId();
		this.task = task;
	}

	@Override
	public String toString() {
		return "TaskResult [id=" + id + ", jobId=" + jobId + ", isSuccess=" + isSuccess + ", execStartTime=" + execStartTime + ", execEndTime=" + execEndTime
				+ ", result=" + result + ", task=" + task + "]";
	}
}
