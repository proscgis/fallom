package com.fallom.transform.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fallom.transform.exception.TaskExecException;

public class Task {
	private String jobId;
	private String id;
	// 节点名称
	private String name;
	// 前置节点
	private List<Task> preTaskList = new ArrayList<Task>();
	// 是否可以忽略该任务的执行异常继续执行该流程:如果为Y，则记录错误信息后继续执行；如果为N，则不再继续执行,整个流程结束
	private String ignoreError = TaskConstant.IGNORE_YES;
	// JAVA、SQL
	private String taskType = TaskConstant.TASKTYPE_SQL;
	// 执行内容
	private String content;
	// 执行参数
	private Map<String, Object> params = new HashMap<String, Object>();;
	private String isEnd;
	// 执行返回结果
	private TaskResult result;
	private JobContext jobContext;

	private static final ThreadLocal<Map<String, Task>> taskMap = new ThreadLocal<Map<String, Task>>();

	private Task() {
	}

	public static Task createTask(String id) {
		if (taskMap.get() == null) {
			taskMap.set(new HashMap<String, Task>());
		}
		if (taskMap.get().containsKey(id)) {
			return taskMap.get().get(id);
		}
		Task task = new Task();
		task.id = id;
		taskMap.get().put(id, task);
		return task;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Task> getPreTaskList() {
		return preTaskList;
	}

	public void setPreTaskList(List<Task> preTaskList) {
		dagCheck(this, preTaskList);
		this.preTaskList = preTaskList;
	}

	private void dagCheck(Task task, List<Task> preTaskList) {
		if (preTaskList != null && preTaskList.size() > 0) {
			for (Task t : preTaskList) {
				if (t.equals(task)) {
					throw new TaskExecException("有环形图:from [" + t + "] to [" + this + "]");
				}
				dagCheck(task, t.getPreTaskList());
			}
		}
	}

	public String getIgnoreError() {
		return ignoreError;
	}

	public boolean isIgnoreError() {
		return TaskConstant.IGNORE_YES.equals(ignoreError);
	}

	public void setIgnoreError(String ignoreError) {
		this.ignoreError = ignoreError;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public void addParam(String key, Object value) {
		this.params.put(key, value);
	}

	public TaskResult getResult() {
		return result;
	}

	public void setResult(TaskResult result) {
		this.result = result;
	}

	public JobContext getJobContext() {
		return jobContext;
	}

	public void setJobContext(JobContext jobContext) {
		this.jobContext = jobContext;
	}

	public static final class TaskConstant {
		public static final String IGNORE_YES = "1";
		public static final String IGNORE_NO = "0";
		public static final String END_YES = "1";
		public static final String END_NO = "0";
		public static final String TASKTYPE_SQL = "SQL";
		public static final String TASKTYPE_JAVA = "JAVA";
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(String isEnd) {
		this.isEnd = isEnd;
	}

	@Override
	public String toString() {
		return "Task [jobId=" + jobId + ", id=" + id + ", name=" + name + ", ignoreError=" + ignoreError + ", taskType=" + taskType + ", content=" + ""
				+ ", params=" + "" + ", isEnd=" + isEnd + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
