package com.fallom.transform.engine.plugin.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;

import com.fallom.transform.engine.JobContext;
import com.fallom.transform.engine.Task;
import com.fallom.transform.engine.TaskExecutor;
import com.fallom.transform.util.JdbcHelper;

public class DbJobContext extends JobContext {
	public DbJobContext(ExecutorService executorService) {
		super(executorService);
	}

	public Connection getConn() {
		Connection conn = null;
		// TODO 改为连接池
		try {
			conn = JdbcHelper.getConn();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public void close(Connection conn) throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}

	@Override
	public TaskExecutor getTaskExecutor(Task task) {
		return new DbTaskExecutor(task);
	}

}
