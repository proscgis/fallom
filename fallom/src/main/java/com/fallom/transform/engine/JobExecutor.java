package com.fallom.transform.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.fallom.transform.exception.TaskExecException;

public class JobExecutor {
	private static final JobExecutor instance = new JobExecutor();

	private JobContext jobContext = null;

	private JobExecutor() {
		try {
			jobContext = (JobContext) Class.forName("com.fallom.transform.engine.plugin.db.DbJobContext").newInstance();
		} catch (Exception e) {
			throw new TaskExecException(e);
		}
	}

	private JobExecutor(ExecutorService es) {
		try {
			// Class.forName("com.fallom.transform.engine.plugin.db.DbJobContext").getConstructor(ExecutorService.class);
			jobContext = (JobContext) Class.forName("com.fallom.transform.engine.plugin.db.DbJobContext").getConstructor(ExecutorService.class).newInstance(es);
		} catch (Exception e) {
			throw new TaskExecException(e);
		}
	}

	public static JobExecutor getInstance() {
		return instance;
	}

	public synchronized void execJob(List<Task> taskDag) {
		List<Future<TaskResult>> futureTaskList = new ArrayList<Future<TaskResult>>();
		for (Task task : taskDag) {
			task.setJobContext(jobContext);
			try {
				futureTaskList.add(jobContext.getTaskExecutor(task).execute());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (Future<TaskResult> t : futureTaskList) {
			TaskResult tr = null;
			try {
				tr = t.get();
				System.out.println("-----" + tr);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void shutdown() {
		jobContext.close();
	}
}
