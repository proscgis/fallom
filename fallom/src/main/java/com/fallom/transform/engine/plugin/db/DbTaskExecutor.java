package com.fallom.transform.engine.plugin.db;

import java.sql.Connection;
import java.sql.Statement;

import com.fallom.transform.engine.Task;
import com.fallom.transform.engine.TaskExecutor;
import com.fallom.transform.engine.TaskResult;

public class DbTaskExecutor extends TaskExecutor {
	public DbTaskExecutor(Task task) {
		super(task);
	}

	public AbstractExecutor getExecutor() {
		return new SqlExecutor();
	}

	public class SqlExecutor extends AbstractExecutor {

		@Override
		public TaskResult call() throws Exception {
			// 前置任务执行完后，执行本次任务
			TaskResult result = new TaskResult();
			result.setExecStartTime(System.currentTimeMillis());
			try {
				doExec(result);
				result.setSuccess(true);
			} catch (Exception e) {
				// e.printStackTrace();
				result = new TaskResult();
				result.setSuccess(false);
				result.setResultErrorMessage(e.getMessage());
			}
			// result.setJobId(jobContext.getJobInstance().getId());
			result.setTask(task);
			task.setResult(result);
			result.setExecEndTime(System.currentTimeMillis());
			System.out.println("============" + result);
			return result;
		}

		private void doExec(TaskResult result) throws Exception {
			String sql = task.getContent();
			if (sql != null) {
				Connection conn = null;
				Statement stat = null;
				DbJobContext jobContext = (DbJobContext) task.getJobContext();
				try {
					conn = jobContext.getConn();
					stat = conn.createStatement();
					System.out.println("执行：" + sql);
					stat.execute(sql);
				} finally {
					if (stat != null) {
						stat.close();
					}
					jobContext.close(conn);
				}
			}
			result.setResult(TaskResult.TaskResultConstant.RESULT_KEY_RETURN_VALUE, sql);
		}
	}

}
