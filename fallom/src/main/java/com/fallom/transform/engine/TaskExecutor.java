package com.fallom.transform.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import com.fallom.transform.exception.TaskExecException;

public abstract class TaskExecutor {
	protected final Task task;
	protected final JobContext jobContext;
	protected final ExecutorService es;

	public TaskExecutor(Task task) {
		super();
		this.task = task;
		this.jobContext = task.getJobContext();
		this.es = jobContext.getExecutorService();
	}

	public Future<TaskResult> execute() {
		// 先执行前置任务
		List<Future<TaskResult>> futureTaskList = new ArrayList<Future<TaskResult>>();
		List<Task> preTaskList = task.getPreTaskList();
		for (Task preTask : preTaskList) {
			preTask.setJobContext(jobContext);
			futureTaskList.add(jobContext.getTaskExecutor(preTask).execute());
		}
		for (Future<TaskResult> t : futureTaskList) {
			TaskResult tr = null;
			try {
				tr = t.get();
				if (tr.isSuccess() || tr.getTask().isIgnoreError()) {
					task.addParam(tr.getTaskId(), tr);
				} else {
					throw new TaskExecException(tr.getResultErrorMessage());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			} catch (Exception e) {
				// e.printStackTrace();
				throw new TaskExecException(e);
			}
		}
		// 是否已经在执行或执行过
		Future<TaskResult> future = jobContext.getCacheTaskResult(task.getId());
		if (future == null) {
			FutureTask<TaskResult> temp = new FutureTask<TaskResult>(getExecutor());
			future = jobContext.putIfAbsentCacheTaskResult(task.getId(), temp);
			if (future == null) {
				future = temp;
				es.submit(temp);
			}
		}
		return future;
	}

	protected abstract AbstractExecutor getExecutor();

	public abstract class AbstractExecutor implements Callable<TaskResult> {
	}

}
