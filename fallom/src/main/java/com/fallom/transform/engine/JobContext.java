package com.fallom.transform.engine;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 每个Job对应一个JobContext，被多个Task共用；每个Task对应一个TaskExecutor，每个TaskExecutor启动一个线程
 * 
 * @author pengrong
 *
 */
public abstract class JobContext {
	private final ExecutorService executorService;

	public JobContext() {
		super();
		this.executorService = Executors.newCachedThreadPool();
	}

	public JobContext(ExecutorService executorService) {
		super();
		this.executorService = executorService;
	}

	private ConcurrentMap<String, Future<TaskResult>> cacheTaskResult = new ConcurrentHashMap<String, Future<TaskResult>>();

	public Future<TaskResult> removeCacheTaskResult(String taskId) {
		return cacheTaskResult.remove(taskId);
	}

	public Future<TaskResult> getCacheTaskResult(String taskId) {
		return cacheTaskResult.get(taskId);
	}

	public Future<TaskResult> putIfAbsentCacheTaskResult(String taskId, Future<TaskResult> taskResult) {
		return this.cacheTaskResult.putIfAbsent(taskId, taskResult);
	}

	public ExecutorService getExecutorService() {
		return executorService;
	}

	public abstract TaskExecutor getTaskExecutor(Task task);

	public void close() {
		executorService.shutdown();
	}
}
