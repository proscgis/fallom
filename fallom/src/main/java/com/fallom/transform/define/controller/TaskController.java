package com.fallom.transform.define.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fallom.transform.define.data.FallomTask;
import com.fallom.transform.define.service.TaskService;

@Controller
@RequestMapping(value = "/web/task")
public class TaskController {
	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/dag")
	@ResponseBody
	public Map<String, Object> getTaskDag() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			List<FallomTask> list = taskService.getTasksOfJob("1");
			resultMap.put("data", list);
			resultMap.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("message", e.getMessage());
			resultMap.put("success", false);
		}
		return resultMap;
	}
}
