package com.fallom.transform.define.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fallom.transform.define.dao.FallomTaskMapper;
import com.fallom.transform.define.data.FallomTask;
import com.fallom.transform.engine.Task;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private FallomTaskMapper taskMapper;

	@Override
	public List<Task> getTaskDag(String jobId) {
		// 1.先查询所有结束节点，每个结束节点对应一个JobInstance
		// 2.查询节点(id=9)的前置节点列表
		// 3.循环2获取节点的前置节点列表，直到前置节点为空
		List<Task> taskList = null;
		taskList = transform(taskMapper.queryEndtask(jobId));
		for (Task t : taskList) {
			setPreTaskList(t);
		}
		return taskList;
	}

	private List<Task> transform(List<FallomTask> sourceList) {
		List<Task> taskList = new ArrayList<Task>();
		for (FallomTask t : sourceList) {
			Task task = Task.createTask(t.getTaskId());
			task.setJobId(t.getJobId());
			task.setId(t.getTaskId());
			task.setName(t.getTaskName());
			task.setContent(t.getTaskContent());
			task.setIsEnd(t.getIsEnd());
			if (t.getInitParams() != null && !"".equals(t.getInitParams())) {
				// task.setParams(Json.);
			}
			task.setIgnoreError(t.getIgnoreError());
			taskList.add(task);
		}
		return taskList;
	}

	private void setPreTaskList(Task task) {
		if (task.getPreTaskList() == null || task.getPreTaskList().isEmpty()) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("jobId", task.getJobId());
			map.put("taskId", task.getId());
			List<Task> preTaskList = transform(taskMapper.queryPretask(map));
			if (preTaskList != null && preTaskList.size() > 0) {
				task.setPreTaskList(preTaskList);
				for (Task t : preTaskList) {
					setPreTaskList(t);
				}
			}
		}
	}

	@Override
	public List<FallomTask> getTasksOfJob(String jobId) {
		return taskMapper.getTasksOfJob(jobId);
	}

}
