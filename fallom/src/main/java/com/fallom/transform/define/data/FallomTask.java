package com.fallom.transform.define.data;

import java.sql.Date;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Table(name = "FALLOM_TASK")
public class FallomTask {
	@Id
	private String taskId; // 主键id
	private String jobId; // job id
	private String taskName; // 专题名称
	private String taskContent; // 内容
	private String isEnd; 
	private String ignoreError; 
	private String initParams; 
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date createtime; // 创建时间
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date updatetime; // 更新时间
	@Transient
	private String preTaskId;
	@Transient
	private String preTaskName;
	
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskContent() {
		return taskContent;
	}
	public void setTaskContent(String taskContent) {
		this.taskContent = taskContent;
	}
	public String getIsEnd() {
		return isEnd;
	}
	public void setIsEnd(String isEnd) {
		this.isEnd = isEnd;
	}
	public String getIgnoreError() {
		return ignoreError;
	}
	public void setIgnoreError(String ignoreError) {
		this.ignoreError = ignoreError;
	}
	public String getInitParams() {
		return initParams;
	}
	public void setInitParams(String initParams) {
		this.initParams = initParams;
	}
	public String getPreTaskId() {
		return preTaskId;
	}
	public void setPreTaskId(String preTaskId) {
		this.preTaskId = preTaskId;
	}
	public String getPreTaskName() {
		return preTaskName;
	}
	public void setPreTaskName(String preTaskName) {
		this.preTaskName = preTaskName;
	}
}
