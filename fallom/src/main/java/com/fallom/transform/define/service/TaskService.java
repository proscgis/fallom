package com.fallom.transform.define.service;

import java.util.List;

import com.fallom.transform.define.data.FallomTask;
import com.fallom.transform.engine.Task;

public interface TaskService {

	public List<FallomTask> getTasksOfJob(String jobId) ;
	public List<Task> getTaskDag(String jobId);
}
