package com.fallom.transform.define.dao;

import java.util.List;
import java.util.Map;

import org.loushang.framework.mybatis.mapper.EntityMapper;

import com.fallom.transform.define.data.FallomTask;

public interface FallomTaskMapper extends EntityMapper<FallomTask> {

	List<FallomTask> queryEndtask(String jobId);

	List<FallomTask> queryPretask(Map<String, String> params);

	List<FallomTask> getTasksOfJob(String jobId);

}
