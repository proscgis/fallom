package com.fallom.transform.test;

import java.util.List;

import com.fallom.transform.define.service.TaskService;
import com.fallom.transform.define.service.TaskServiceImpl;
import com.fallom.transform.engine.JobExecutor;
import com.fallom.transform.engine.Task;

public class Main1 {

	public static void main(String[] args) {
		TaskService taskService = new TaskServiceImpl();
		List<Task> endTaskList = taskService.getTaskDag("1");
		JobExecutor jobExecutor = JobExecutor.getInstance();
		jobExecutor.execJob(endTaskList);
		jobExecutor.shutdown();
	}

}
